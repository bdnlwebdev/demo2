import * as functions from "firebase-functions";
import { Firestore } from "@google-cloud/firestore";
import * as Parser from "rss-parser";
import { Md5 } from "ts-md5/dist/md5";

export const getNews = functions.https.onRequest(async (request, response) => {
  const db = new Firestore();
  const parser = new Parser();

  await verwerkFeed("Tweakers nieuws", "http://feeds.feedburner.com/tweakers/nieuws", parser, db);
  await verwerkFeed("Tweakers mixed", "http://feeds.feedburner.com/tweakers/mixed", parser, db);
  await verwerkFeed("Tweakers meuktracker", "http://feeds.feedburner.com/tweakers/meuktracker", parser, db);

  await verwerkFeed("nu.nl Algemeen", "https://www.nu.nl/rss/Algemeen", parser, db);
  await verwerkFeed("nu.nl Wetenschap", "https://www.nu.nl/rss/Wetenschap", parser, db);

  await verwerkFeed("security.nl", "https://www.security.nl/rss/headlines.xml", parser, db);
  await verwerkFeed("Krantenkoppen", "http://www.krantenkoppen.eu/feed/", parser, db);

  await verwerkFeed("JavaWorld", "https://www.javaworld.com/index.rss", parser, db);
  await verwerkFeed("Java Code Geeks", "https://www.javacodegeeks.com/feed", parser, db);
  await verwerkFeed("Plumbr Java Performance Monitoring", "https://plumbr.io/blog/feed", parser, db);
  await verwerkFeed("Java geek", "https://blog.frankel.ch/feed.xml", parser, db);

  // await cleanOld(db);
  response.set('Access-Control-Allow-Origin', '*');
  response.status(200).send("Gelukt!");
});

async function verwerkFeed(feedTitle: string, url: string, parser: Parser, db: Firestore) {
  console.log(`Lezen ${feedTitle}`)
  const feed:Parser.Output = await parser.parseURL(url);
  if (typeof feed === "object") {
      console.log(`Verwerken ${feedTitle}`)
      feed.items?.forEach((value: Parser.Item) => {
        saveNews(db, value, feedTitle);
      });
  }
}

// async function cleanOld(db: Firestore) {
//   await db.collection('nieuws').get().then(async snapCount => {
//     if (snapCount.size > 15) {
//       await db.collection('nieuws').orderBy('date', 'asc').limit(snapCount.size - 15).get().then(snap=>{
//         snap.forEach(async doc=>{
//           await doc.ref.delete();
//         })
//       })
//     }
//   });
// }

function saveNews(db: Firestore, item: Parser.Item, feed: string) {
  console.log(item);
  const strId = Md5.hashAsciiStr(item.guid || "no guid");
  if (typeof strId === "string") {
    db.collection("nieuws")
      .doc(strId)
      .set({
        feed: feed,
        title: item.title,
        date: item.isoDate,
        item: item,
      })
      .then(ref => {
        console.log(`Added document ${item.guid} as ${strId}`);
      })
      .catch(error => {
        console.log(error);
      });
  }
}
