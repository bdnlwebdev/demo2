// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAEyOvq_Qt3Y5GGX-3G57c4fb5eUGQ_q0U',
    authDomain: 'bd-demo2.firebaseapp.com',
    databaseURL: 'https://bd-demo2.firebaseio.com',
    projectId: 'bd-demo2',
    storageBucket: 'bd-demo2.appspot.com',
    messagingSenderId: '394966921215',
    appId: '1:394966921215:web:f41cbf9c7a1cd61ea401ca'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
