import { Component } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import 'firebase/firestore';
import { Observable } from 'rxjs';
import { Snelheid } from './snelheid.interface';
import { Item } from './item.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  snelheid:  AngularFirestoreDocument<Snelheid>;
  changes: Observable<Snelheid>;
  currentSnelheid: number;
  itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;

  constructor(firestore: AngularFirestore) {
    this.snelheid = firestore.doc<Snelheid>('snelheid/1');
    this.changes = this.snelheid.valueChanges();
    this.changes.subscribe(s=>{
      this.currentSnelheid = s.snelheid;
    })
    this.itemsCollection = firestore.collection<Item>('nieuws', ref => ref.orderBy('date', 'desc').limit(15));
    this.items = this.itemsCollection.valueChanges();
  }
  plus() {
    this.snelheid.update({snelheid: this.currentSnelheid + 1})
  }
  min() {
    this.snelheid.update({snelheid: this.currentSnelheid - 1})
  }
}
